#!/bin/bash
# run local test server on specified port

PORT = 8000

python -m http.server $PORT --bind 127.0.0.1
