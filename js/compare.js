/*
 * Copyright (c) 2019-2020, Lukas Sigrist <lsigrist (at) mailbox.org>
 * All rights reserved.
 * 
 * Inspired by the image zoom example at <https://www.w3schools.com/howto/howto_js_image_zoom.asp>.
 */

/// Initialize comparison
function init_compare(preview_data, details_data) {
  var preview;
  var preview_exif;
  var lens;
  var details = [];
  var details_exif = [];

  preview = document.getElementById(preview_data.id);
  preview.src = preview_data.src;

  EXIF.getData(preview_data, function() {
    preview_exif = EXIF.getAllTags(this);

    // create lens
    lens = document.createElement("DIV");
    lens.setAttribute("class", "image-zoom-lens");
    lens.style.width = (preview.width / preview_exif.PixelXDimension) * preview.width + "px";
    lens.style.height = (preview.height / preview_exif.PixelYDimension) * preview.height + "px";

    // insert lens
    preview.parentElement.insertBefore(lens, preview);

    // execute a function when someone moves the cursor over the image, or the lens
    lens.addEventListener("mousemove", update_lens);
    preview.addEventListener("mousemove", update_lens);
    // and also for touch screens
    lens.addEventListener("touchmove", update_lens);
    preview.addEventListener("touchmove", update_lens);

    for (var i = 0; i < details_data.length; i++) {
      EXIF.getData(details_data[i], function() {
        // extract detail element and EXIF
        detail = document.getElementById(this.id);
        detail_caption = detail.parentElement.getElementsByTagName("figcaption")[0];
        detail_exif = EXIF.getAllTags(this);

        // lens make is listed as 'undefined'
        detail_caption.innerHTML = detail_exif.undefined + "<br />\n" + 
          detail_exif.FocalLength + " mm | f/" + detail_exif.FNumber + 
          " | " + detail_exif.ExposureTime + " s | ISO " + detail_exif.ISOSpeedRatings;

        // keep track of detail info for updates
        details.push(detail);
        details_exif.push(detail_exif);

        // scale image to 100 percent view of preview using focal length
        detail_scaling = preview_exif.FocalLength / detail_exif.FocalLength;
        detail_width = detail_scaling * detail_exif.PixelXDimension;
        detail_height = detail_scaling * detail_exif.PixelYDimension;

        // set background properties
        detail.style.backgroundImage = "url('" + this.src + "')";
        detail.style.backgroundSize = detail_width + "px " + detail_height + "px";
      });
    }
  });

  /// Update the lens position and detail images on mouse move event
  function update_lens(event) {
    // suppress any other actions
    event.preventDefault();
    // get the cursor position
    var pos = preview_cursor_position(event);

    // calculate lens position
    var x = pos.x - (lens.offsetWidth / 2);
    var y = pos.y - (lens.offsetHeight / 2);

    // prevent the lens position overflow
    if (x > preview.clientWidth - lens.offsetWidth) { x = preview.clientWidth - lens.offsetWidth; }
    if (x < 0) { x = 0; }
    if (y > preview.clientHeight - lens.offsetHeight) { y = preview.clientHeight - lens.offsetHeight; }
    if (y < 0) { y = 0; }

    // update lens position
    lens.style.left = x + "px";
    lens.style.top = y + "px";

    // get relative lens positioning
    var x_rel = x / (preview.clientWidth - lens.offsetWidth);
    var y_rel = y / (preview.clientHeight - lens.offsetHeight);

    // adjust detail views positioning
    for (var i = 0; i < details.length; i++) {
      // scale image to 100 percent view of preview using focal length
      var base_scaling = preview_exif.FocalLength / details_exif[i].FocalLength;
      detail_posX = (base_scaling * details_exif[i].PixelXDimension - details[i].clientWidth) * x_rel;
      detail_posY = (base_scaling * details_exif[i].PixelYDimension - details[i].clientHeight) * y_rel;
      details[i].style.backgroundPosition = "-" + detail_posX + "px -" + detail_posY + "px";
    }
  }

  /// Get the cursor position within the preview image
  function preview_cursor_position(event) {
    var preview_box, x = 0, y = 0;
    event = event || window.event;
    // get the x and y positions of the image
    preview_box = preview.getBoundingClientRect();
    // calculate the cursor's x and y coordinates, relative to the image:
    x = event.pageX - preview_box.left;
    y = event.pageY - preview_box.top;
    // consider any page scrolling
    x = x - window.pageXOffset;
    y = y - window.pageYOffset;
    return { x: x, y: y };
  }
}

/// Initialize after document loaded and ready
$(function () {
  var compare_preview = { id: "preview", src: "media/2019-08-31_RAW_0001.jpg" };
  var compare_details = [
    { id: "detail1", src: "media/2019-08-31_RAW_0002.jpg" },
    { id: "detail2", src: "media/2019-08-31_RAW_0004.jpg" },
    { id: "detail3", src: "media/2019-08-31_RAW_0054.jpg" },
    { id: "detail4", src: "media/2019-08-31_RAW_0056.jpg" }
  ];
  init_compare(compare_preview, compare_details);
});
